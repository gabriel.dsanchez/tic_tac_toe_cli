"""
tic_tac_toe_cli is a Tic-Tac-Toe game ran in CLI

Call this script directly to start game
"""

class Game_Board(object):
    def __init__(self):
        self.game_board = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]

    def set_game_board_cell(self, row, column, value):
        """
        Set cell value.
        """
        could_set = False

        if self.game_board[row][column] == ' ':
            self.game_board[row][column] = value
            could_set = True
        
        return could_set
    
    def get_game_board_cell(self, row, column):
        """
        Return cell value.
        """
        return self.game_board[row][column]
    
    def get_game_board_row(self, row):
        """
        Return full row list.
        """
        return self.game_board[row]
    
    def get_game_board_column(self, column):
        """
        Return full column list from matrix.
        """
        return [self.game_board[0][column-1], self.game_board[1][column-1], self.game_board[2][column-1]]

def _get_player_one_flag():
    """
    Get player one flag as 'X' or 'O'
    """

    correct_input = False

    while not correct_input:
        player_one_flag = input("Please, enter player one's flag as either 'X' or 'O': ")
        if player_one_flag.upper() == 'X':
            player_two_flag = 'O'
            correct_input = True
        elif player_one_flag.upper() == 'O':
            player_two_flag = 'X'
            correct_input = True
        else:
            print("Wrong input!")
    
    return (player_one_flag.upper(), player_two_flag.upper())

def _get_target_field():
    """
    Get player's target field in the board.
    
    Matrix follows numpad layout.
    """

    correct_input = False

    while not correct_input:
        try:
            player_target = int(input("Choose a target field (1 - 9): "))
            if player_target in range(1,10):
                correct_input = True
            else:
                print("Pick a number from range: [1,2,3,4,5,6,7,8,9]!")
        except:
            print("Wrong input!")
    
    return player_target

def _try_apply_target_for_player(matrix, target_field, player_flag):
    """
    Apply player flag into target field.
    """

    could_set = False
    
    if target_field in range(1,4):
        could_set = matrix.set_game_board_cell(2, target_field-1, player_flag)
    elif target_field in range(4,7):
        could_set = matrix.set_game_board_cell(1, target_field-4, player_flag)
    else:
        could_set = matrix.set_game_board_cell(0, target_field-7, player_flag)

    return matrix, could_set

def _player_interaction(matrix, player_flag):
    """
    Couple together player interaction methods.
    """
    successful_interaction = False
    while not successful_interaction:
        target_field = _get_target_field()

        matrix, could_set = _try_apply_target_for_player(matrix, target_field, player_flag)

        if could_set:
            successful_interaction = True
        else:
            print("Please, pick a cell that is available!")

            _print_game_board(matrix)
    
    return matrix

def _check_winner(matrix, player_flag):
    """
    Checks if there's a winner based on the player flag.
    """

    row1 = ''.join(matrix.get_game_board_row(0))
    row2 = ''.join(matrix.get_game_board_row(1))
    row3 = ''.join(matrix.get_game_board_row(2))
    column1 = ''.join(matrix.get_game_board_column(0))
    column2 = ''.join(matrix.get_game_board_column(1))
    column3 = ''.join(matrix.get_game_board_column(2))
    diagonal1 = matrix.get_game_board_cell(0, 0) + matrix.get_game_board_cell(1, 1) + matrix.get_game_board_cell(2, 2)
    diagonal2 = matrix.get_game_board_cell(0, 2) + matrix.get_game_board_cell(1, 1) + matrix.get_game_board_cell(2, 0)

    check_string = ' '.join([row1, row2, row3, column1, column2, column3, diagonal1, diagonal2]) 

    count_win_string = check_string.count(player_flag * 3)

    return count_win_string

def _check_playability(matrix):
    """
    Check if game board is full.
    """

    row1 = ''.join(matrix.get_game_board_row(0))
    row2 = ''.join(matrix.get_game_board_row(1))
    row3 = ''.join(matrix.get_game_board_row(2))

    check_string = row1 + row2 + row3

    return check_string.count(" ")

def _print_game_board(matrix):
    """
    Print formatted game board
    """

    row1 = ''.join(matrix.get_game_board_row(0))
    row2 = ''.join(matrix.get_game_board_row(1))
    row3 = ''.join(matrix.get_game_board_row(2))

    print("Game board:")
    print(" | ".join(row1))
    print("---------")
    print(" | ".join(row2))
    print("---------")
    print(" | ".join(row3))

def _get_continue_playing():
    """
    Check if users want to continue playing.
    """
    while True:
        answer = input("Do you want to play again? (Y or N): ")

        if answer.lower() == 'y':
            return True
        elif answer.lower() == 'n':
            return False
        else:
            print("Wrong input! Please, use either 'Y' or 'N'!")

if __name__ == '__main__':
    """
    Script should be called directly.
    """

    player_one_win_count = 0
    player_two_win_count = 0

    continue_playing = True

    while continue_playing:
        game_matrix = Game_Board()

        introduction = """CLI Tic-Tac-Toe

        Basic tic-tac-toe game via CLI. Refer to your numpad keyboard to interact with cells.

        7 | 8 | 9
        ---------
        4 | 5 | 6
        ---------
        1 | 2 | 3
        """

        print(introduction)

        print("Game score:\nPlayer One {}\nPlayer Two {}".format(player_one_win_count, player_two_win_count))

        player_one_flag, player_two_flag = _get_player_one_flag()

        available_cells = 9
        while available_cells > 0:
            print("Player one...")

            game_matrix = _player_interaction(game_matrix, player_one_flag)

            player_one_winner_string = _check_winner(game_matrix, player_one_flag)

            if player_one_winner_string == 0:
                available_cells = _check_playability(game_matrix)

                _print_game_board(game_matrix)

                if available_cells > 0:
                    print("Player two...")

                    game_matrix = _player_interaction(game_matrix, player_two_flag)

                    player_two_winner_string = _check_winner(game_matrix, player_two_flag)

                    if player_two_winner_string == 0:
                        available_cells = _check_playability(game_matrix)
                        _print_game_board(game_matrix)
                        if available_cells == 0:
                            print("No more available cells! DRAW! ! !")                            
                    else:
                        print("PLAYER TWO IS THE WINNER! ! !")
                        available_cells = 0
                        player_two_win_count += 1
                        _print_game_board(game_matrix)
                else:
                    print("No more available cells! DRAW! ! !")
            else:
                print("PLAYER ONE IS THE WINNER! ! !")
                available_cells = 0
                player_one_win_count += 1
                _print_game_board(game_matrix)

        continue_playing = _get_continue_playing()