# CLI TIC-TAC-TOE *<small>v. 1.0.0</small>*

Basic Tic-Tac-Toe game via CLI.

Created for training purposes with Python 3.8.

*Known possible improvements:*

  * Create computer logic (AI) to play with lone player.